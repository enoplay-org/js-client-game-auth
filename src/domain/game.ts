interface GameRepository {
  getByUID (uid: string): Promise<Game>
}

class Game {
  uid: string
  gid: string
  title: string
  description: string
  media: {
    thumbnail: {
      source: string
    }
  }
  publisher: {
    uid: string
    username: string
  }
}

export {
  GameRepository,
  Game
}
