import { GameRepository, Game } from '../domain'

class GameInteractorOptions { }

class GameInteractor {
  options: GameInteractorOptions
  gameRepository: GameRepository
  constructor (options: GameInteractorOptions) {
    this.options = options
  }

  setGameRepository (gameRepository: GameRepository) {
    this.gameRepository = gameRepository
  }

  getByUID (uid: string): Promise<Game> {
    return this.gameRepository.getByUID(uid)
  }
}

export {
  GameInteractorOptions,
  GameInteractor
}
