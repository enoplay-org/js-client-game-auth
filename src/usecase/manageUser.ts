import { UserRepository, User } from '../domain'

class UserInteractorOptions { }

class UserInteractor {
  options: UserInteractorOptions
  userRepository: UserRepository
  constructor (options: UserInteractorOptions) {
    this.options = options
  }

  setUserRepository (userRepository: UserRepository) {
    this.userRepository = userRepository
  }

  getByUID (uid: string): Promise<User> {
    return this.userRepository.getByUID(uid)
  }
}

export {
  UserInteractorOptions,
  UserInteractor
}
