import axios from 'axios'

import { Game } from '../../domain'

class GameRepoOptions {
  apiHostURL: string
  constructor (apiHostURL: string) {
    this.apiHostURL = apiHostURL
  }
}

class GameRepo {
  options: GameRepoOptions
  constructor (options: GameRepoOptions) {
    this.options = options
  }

  getByUID (uid: string): Promise<Game> {
    return new Promise((resolve, reject) => {
      return axios.get(`${this.options.apiHostURL}/games?field=uid&q=${uid}`)
      .then(response => {
        let data = response.data

        if (!data.success) {
          return Promise.reject(`Error: Expected response from apiHost`)
        }

        let game = data.games[0]
        return game
      })
      .then(gameResponse => {
        resolve(gameResponse)
      })
      .catch(err => {
        reject(`Error retrieving game from apiHost: ${this.options.apiHostURL}\n\n${err}`)
      })
    })
  }
}

export {
  GameRepoOptions,
  GameRepo
}
