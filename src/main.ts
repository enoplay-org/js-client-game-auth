
import {
  FrameHandlerOptions,
  FrameHandler
} from './interfaces/frame'

import {
  DomHandlerOptions,
  DomHandler
} from './interfaces/dom'

import {
  PlayTokenRepoOptions,
  PlayTokenRepo,
  GameRepoOptions,
  GameRepo,
  UserRepoOptions,
  UserRepo
} from './interfaces/repository'

import {
  PlayTokenInteractorOptions,
  PlayTokenInteractor,
  GameInteractorOptions,
  GameInteractor,
  UserInteractorOptions,
  UserInteractor
} from './usecase'

import {
  PlayTokenClaim
} from './domain'

// Setup expected environment variables
if (!(<any>window).enoplayAuthEnvV0) {
  (<any>window).enoplayAuthEnvV0 = {}
}

const ENV_GAME_UID = (<any>window).enoplayAuthEnvV0.gameUID || ''
const ENV_HOST_URL_BETA = 'https://beta.enoplay.com'
const ENV_HOST_URL_WWW = (<any>window).enoplayAuthEnvV0.wwwHostURL || ''
const ENV_HOST_URL_MOBILE = (<any>window).enoplayAuthEnvV0.mobileHostURL || ''
const ENV_HOST_URL_API = (<any>window).enoplayAuthEnvV0.apiHostURL || ''
const ENV_HOST_URL_CDN = (<any>window).enoplayAuthEnvV0.cdnHostURL || ''
const ENV_HOST_URL_CURRENT = location.origin || ''

// Is the current window ep-index.html? Else assumet it is index.html
const ENV_IS_AUTH_INDEX: boolean = (<any>window).enoplayAuthEnvV0.isAuthIndex ? true : false

// Setup Play Token repository & interactor
let playTokenRepoOptions = new PlayTokenRepoOptions(
  `${ENV_HOST_URL_CURRENT}/enoplay/plays/token_verification`
)

let playTokenRepo = new PlayTokenRepo(playTokenRepoOptions)

let playTokenInteractorOptions = new PlayTokenInteractorOptions(ENV_IS_AUTH_INDEX)
let playTokenInteractor = new PlayTokenInteractor(playTokenInteractorOptions)
playTokenInteractor.setPlayTokenRepository(playTokenRepo)


// Setup Game repository & interactor
let gameRepoOptions = new GameRepoOptions(ENV_HOST_URL_API)
let gameRepo = new GameRepo(gameRepoOptions)

let gameInteractorOptions = new GameInteractorOptions()
let gameInteractor = new GameInteractor(gameInteractorOptions)
gameInteractor.setGameRepository(gameRepo)

// Setup User repository & interactor
let userRepoOptions = new UserRepoOptions(ENV_HOST_URL_API)
let userRepo = new UserRepo(userRepoOptions)

let userInteractorOptions = new UserInteractorOptions()
let userInteractor = new UserInteractor(userInteractorOptions)
userInteractor.setUserRepository(userRepo)

// Setup FrameHandler for managing communication between the current window and parent frame
let frameHandlerOptions = new FrameHandlerOptions(
  ENV_HOST_URL_WWW,
  ENV_HOST_URL_MOBILE,
  ENV_HOST_URL_BETA
)

let frameHandler = new FrameHandler(frameHandlerOptions)

// Handle verifyToken message
frameHandler.addMessageHandler('verifyToken', (data: any) => {
  let playTokenClaim = new PlayTokenClaim(data['token'])
  playTokenInteractor.verify(playTokenClaim)
})

// Handle error message
frameHandler.addMessageHandler('error', (data: any) => {
  console.warn(`Error from parent frame: ${data.description}`)
})

// Let's do this!
frameHandler.listenForMessage()

// Request a play token
frameHandler.sendMessage('tokenRequest', '')

// Request a play token every 30 minutes
frameHandler.scheduleMessage('tokenRequest', '', 30)

// Setup DomHandler for managing interaction with the document object
let domHandleOptions = new DomHandlerOptions()
let domHandler = new DomHandler(domHandleOptions)

// Populate dom with game details
gameInteractor.getByUID(ENV_GAME_UID)
.then((gameResponse) => {
  return userInteractor.getByUID(gameResponse.publisher.uid)
  .then((userResponse) => {
    domHandler.updateElementStyle('gameThumbnail', `background-image: url(${gameResponse.media.thumbnail.source});`)
    domHandler.updateElementInnerHTML('gameTitle', gameResponse.title)
    domHandler.updateElementHref('publisherLinkIcon', `${ENV_HOST_URL_WWW}/${userResponse.username}`)
    domHandler.updateElementStyle('publisherIcon', `background-image: url(${userResponse.media.icon.source});`)
    domHandler.updateElementHref('publisherLinkName', `${ENV_HOST_URL_WWW}/${userResponse.username}`)
    domHandler.updateElementInnerHTML('publisherAlias', `${userResponse.alias}`)
    domHandler.updateElementInnerHTML('publisherUsername', `@${userResponse.username}`)
    domHandler.updateElementHref('wwwLinkLogo', ENV_HOST_URL_WWW)
    domHandler.updateElementSrc('wwwLogo', `${ENV_HOST_URL_CDN}/img/client/game/overlay-logo.png`)
  })
})
.catch(err => {
  console.warn(`Error populating game details for gameUID: ${ENV_GAME_UID}\n\n${err}`)
})
