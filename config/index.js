// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')

module.exports = {
  build: {
    fileName: 'auth-game-client.js',
    distPath: path.resolve(__dirname, '../dist'),
    testPaths: [
      path.resolve(__dirname, '../test/integration/frame-child/js')
    ]
  }
}
