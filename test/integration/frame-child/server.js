var path = require('path')
var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')

var server = express()

server.use(cors())
server.use(express.static(__dirname))
server.use(bodyParser.json())

server.post('/ep_verify_token', function (req, res) {
  res.json({
    token: req.body,
    message: 'User is verified!',
    success: true
  })
})

server.get('/*', function (req, res) {
  res.sendFile(path.resolve(__dirname, './index.html'))
})

var port = process.env.PORT || 7701
server.listen(port, function () {
  console.log('child frame: server listening on port ' + port)
})
